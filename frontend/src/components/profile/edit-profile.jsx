import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Form, Icon, Image, ImageUpload, Message, Modal, Label,
  LimitedTextarea } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { ButtonColor, ButtonSize, ButtonType, IconName } from 'src/common/enums/enums';
import PropTypes from 'prop-types';
import { profileActionCreator } from 'src/store/actions';
import styles from './styles.module.scss';
import { Grid } from '../common/common';

const EditProfile = ({ close }) => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));

  const [image, setImage] = React.useState(user.image);
  const [username, setUsername] = React.useState(user.username);
  const [isUsernameValid, setIsUsernameValid] = React.useState(true);
  const [error, setError] = React.useState(undefined);
  const [isEditAvatar, setIsEditAvatar] = React.useState(false);
  const [status, setStatus] = React.useState(user.status);

  const dispatch = useDispatch();

  const onEditProfile = React.useCallback(postPayload => (
    dispatch(profileActionCreator.updateUser(postPayload))
  ), [dispatch]);

  const handleUserUpdate = React.useCallback(
    newUser => dispatch(
      profileActionCreator.setUser(newUser)
    ),
    [dispatch]
  );

  const usernameChanged = data => {
    setUsername(data);
    setIsUsernameValid(true);
  };

  const handleEditProfile = async () => {
    if (!isUsernameValid) {
      return;
    }
    const isDataChanged = user.username !== username
        || image?.id !== user.image?.id || user.status !== status;
    if (!isDataChanged) {
      close();
      return;
    }
    const response = await onEditProfile({ userId: user.id, username, imageId: image?.id, status });
    setError(response.error);
    if (!response.error) {
      close();
      setUsername('');
      setImage(undefined);
      handleUserUpdate(response.user);
    }
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Edit profile</span>
      </Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleEditProfile}>
          <Grid container textAlign="center">
            <Grid.Column>
              <Image
                centered
                src={image?.link ?? DEFAULT_USER_AVATAR}
                size="medium"
                circular
              />
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => setIsEditAvatar(true)}
              >
                <Icon name={IconName.EDIT} />
                {user.image?.link ? 'edit image' : 'add image'}
              </Label>
              <Form.Input
                style={{ width: '250px' }}
                icon="user"
                iconPosition="left"
                placeholder="Username"
                type="text"
                error={!isUsernameValid}
                value={username}
                onChange={ev => usernameChanged(ev.target.value)}
                onBlur={() => setIsUsernameValid(Boolean(username))}
              />
              <LimitedTextarea
                style={{ width: '250px', height: '20px' }}
                value={status}
                placeholder="Type a status..."
                onChange={text => setStatus(text)}
                limit={99}
              />
              <Button
                type={ButtonType.SUBMIT}
                color={ButtonColor.TEAL}
                size={ButtonSize.LARGE}
                isPrimary
              >
                Save
              </Button>
            </Grid.Column>
          </Grid>
        </Form>
        {error && (
          <Message color="red">
            {error}
          </Message>
        )}
        {isEditAvatar && (
          <ImageUpload
            onImageSaved={img => setImage(img)}
            close={() => setIsEditAvatar(false)}
          />
        )}
      </Modal.Content>
    </Modal>
  );
};

EditProfile.propTypes = {
  close: PropTypes.func.isRequired
};

export default EditProfile;
