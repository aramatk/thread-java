import * as React from 'react';
import { useSelector } from 'react-redux';
import { Grid, Icon, Image, Input, Label, Message } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { IconName } from 'src/common/enums/components/icon-name.enum';
import EditProfile from './edit-profile';
import styles from './styles.module.scss';

const Profile = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  const [isEdit, setIsEdit] = React.useState(false);

  return (
    <>
      <Grid container textAlign="center" style={{ paddingTop: 30 }}>
        <Grid.Column>
          <Image
            centered
            src={user.image?.link ?? DEFAULT_USER_AVATAR}
            size="medium"
            circular
          />
          <br />
          <Input
            style={{ width: '250px' }}
            icon="user"
            iconPosition="left"
            placeholder="Username"
            type="text"
            disabled
            value={user.username}
          />
          <br />
          <br />
          <Input
            style={{ width: '250px' }}
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            disabled
            value={user.email}
          />
          <br />
          {user.status && (
            <Message style={{ width: '250px' }} compact>
              {user.status}
            </Message>
          )}
          <br />
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => setIsEdit(true)}
          >
            <Icon name={IconName.EDIT} />
            Edit Profile
          </Label>
        </Grid.Column>
      </Grid>
      {isEdit && (
        <EditProfile close={() => setIsEdit(false)} />
      )}
    </>
  );
};

export default Profile;
