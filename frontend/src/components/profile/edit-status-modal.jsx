import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Form, LimitedTextarea, Modal, Grid } from 'src/components/common/common';
import { ButtonColor, ButtonSize, ButtonType } from 'src/common/enums/enums';
import PropTypes from 'prop-types';
import { profileActionCreator } from 'src/store/actions';
import styles from './styles.module.scss';

const EditStatusModal = ({ close }) => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));

  const [status, setStatus] = React.useState(user.status);

  const dispatch = useDispatch();

  const onUpdateStatus = React.useCallback(postPayload => (
    dispatch(profileActionCreator.updateUser(postPayload))
  ), [dispatch]);

  const handleUserUpdate = React.useCallback(
    newUser => dispatch(
      profileActionCreator.setUser(newUser)
    ),
    [dispatch]
  );

  const handleUpdateStatus = async () => {
    const isDataChanged = user.status !== status;
    if (!isDataChanged) {
      close();
      return;
    }
    const response = await onUpdateStatus(
      { userId: user.id, username: user.username, imageId: user.image?.id, status }
    );
    handleUserUpdate(response.user);
    close();
  };

  return (
    <Modal open onClose={close} size="mini">
      <Modal.Header className={styles.header}>
        <span>Update status</span>
      </Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleUpdateStatus}>
          <Grid container textAlign="center">
            <Grid.Column>
              <LimitedTextarea
                style={{ width: '250px', height: '20px' }}
                value={status}
                placeholder="Type a status..."
                onChange={text => setStatus(text)}
                limit={99}
              />
              <Button
                type={ButtonType.SUBMIT}
                color={ButtonColor.TEAL}
                size={ButtonSize.LARGE}
                isPrimary
              >
                Save
              </Button>
            </Grid.Column>
          </Grid>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

EditStatusModal.propTypes = {
  close: PropTypes.func.isRequired
};

export default EditStatusModal;
