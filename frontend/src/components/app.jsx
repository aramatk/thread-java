import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { AppRoute, StorageKey } from 'src/common/enums/enums';
import { storage } from 'src/services/services';
import { profileActionCreator, threadActionCreator } from 'src/store/actions';
import { Header, Notifications, PrivateRoute, PublicRoute, Spinner } from 'src/components/common/common';
import SignPage from 'src/components/sign/sign';
import NotFoundPage from 'src/components/not-found/not-found';
import ProfilePage from 'src/components/profile/profile';
import SharedPostPage from 'src/components/shared-post/shared-post';
import ThreadPage from 'src/components/thread/thread';
import ChangePasswordForm from 'src/components/change-password-form/change-password-form';

const Routing = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  const dispatch = useDispatch();

  const hasToken = Boolean(storage.getItem(StorageKey.TOKEN));
  const hasUser = Boolean(user);

  const handlePostApply = React.useCallback(id => (
    dispatch(threadActionCreator.applyPost(id))
  ), [dispatch]);

  const handleUpdateEditedPost = React.useCallback(id => (
    dispatch(threadActionCreator.updateEditedPost(id))
  ), [dispatch]);

  const handleUpdateRemovedPost = React.useCallback(id => (
    dispatch(threadActionCreator.updateRemovedPost(id))
  ), [dispatch]);

  const handleCommentApply = React.useCallback(id => (
    dispatch(threadActionCreator.applyComment(id))
  ), [dispatch]);

  const handleUpdateEditedComment = React.useCallback(id => (
    dispatch(threadActionCreator.updateEditedComment(id))
  ), [dispatch]);

  const handleUpdateRemovedComment = React.useCallback(id => (
    dispatch(threadActionCreator.updateRemovedComment(id))
  ), [dispatch]);

  const handleUpdatePostReaction = React.useCallback((reaction, postId, isLike) => (
    dispatch(threadActionCreator.updateReactPost(reaction, postId, isLike))
  ), [dispatch]);

  const handleUpdateCommentReaction = React.useCallback((reaction, commentId, isLike) => (
    dispatch(threadActionCreator.updateReactComment(reaction, commentId, isLike))
  ), [dispatch]);

  const handleUserLogout = React.useCallback(() => (
    dispatch(profileActionCreator.logout())
  ), [dispatch]);

  React.useEffect(() => {
    if (hasToken) {
      dispatch(profileActionCreator.loadCurrentUser());
    }
  }, [hasToken, dispatch]);

  if (!hasUser && hasToken) {
    return <Spinner isOverflow />;
  }

  return (
    <div className="fill">
      {hasUser && (
        <header>
          <Header user={user} onUserLogout={handleUserLogout} />
        </header>
      )}
      <main className="fill">
        <Switch>
          <PublicRoute
            exact
            path={[AppRoute.LOGIN, AppRoute.REGISTRATION, AppRoute.RESET_PASSWORD]}
            component={SignPage}
          />
          <PublicRoute
            exact
            path={AppRoute.RESET_PASSWORD_$TOKEN}
            component={ChangePasswordForm}
          />
          <PrivateRoute exact path={AppRoute.ROOT} component={ThreadPage} />
          <PrivateRoute exact path={AppRoute.PROFILE} component={ProfilePage} />
          <PrivateRoute path={AppRoute.SHARE_$POSTHASH} component={SharedPostPage} />
          <Route path={AppRoute.ANY} exact component={NotFoundPage} />
        </Switch>
      </main>
      <Notifications
        user={user}
        onPostApply={handlePostApply}
        onPostUpdate={handleUpdateEditedPost}
        onPostRemove={handleUpdateRemovedPost}
        onCommentApply={handleCommentApply}
        onCommentUpdate={handleUpdateEditedComment}
        onCommentRemove={handleUpdateRemovedComment}
        onPostReactionUpdate={handleUpdatePostReaction}
        onCommentReactionUpdate={handleUpdateCommentReaction}
      />
    </div>
  );
};

export default Routing;
