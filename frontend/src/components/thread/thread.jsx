import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { threadActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import { Checkbox, Post, Spinner } from 'src/components/common/common';
import { AddPost, ExpandedPost, SharedPostLink, SharedPostMail } from './components/components';
import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10,
  excludeUserId: undefined,
  showLikedByUserId: undefined
};

const Thread = () => {
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [sharedCopyPostId, setSharedCopyPostId] = React.useState(undefined);
  const [sharedMailPostId, setSharedMailPostId] = React.useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [hideOwnPosts, setHideOwnPosts] = React.useState(false);
  const [showLikedByMePosts, setShowLikedByMePosts] = React.useState(false);

  const dispatch = useDispatch();

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.reactPost(id, true))
  ), [dispatch]);

  const handlePostDislike = React.useCallback(id => (
    dispatch(threadActionCreator.reactPost(id, false))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handlePostAdd = React.useCallback(postPayload => (
    dispatch(threadActionCreator.createPost(postPayload))
  ), [dispatch]);

  const handlePostEdit = React.useCallback(postPayload => (
    dispatch(threadActionCreator.editPost(postPayload))
  ), [dispatch]);

  const handlePostDelete = React.useCallback(id => (
    dispatch(threadActionCreator.deletePost(id))
  ), [dispatch]);

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMorePosts(filtersPayload));
  };

  const handleLoadLikedUsers = React.useCallback(id => (
    dispatch(threadActionCreator.loadUsersPostReactions(id, true))
  ), [dispatch]);

  const handleLoadDislikedUsers = React.useCallback(id => (
    dispatch(threadActionCreator.loadUsersPostReactions(id, false))
  ), [dispatch]);

  const handleSharePostByEmail = React.useCallback(request => {
    dispatch(threadActionCreator.sendEmail(request));
  },
  [dispatch]);

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    setHideOwnPosts(false);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    postsFilter.excludeUserId = undefined;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleHideOwnPosts = () => {
    setHideOwnPosts(!hideOwnPosts);
    setShowOwnPosts(false);
    postsFilter.userId = undefined;
    postsFilter.excludeUserId = hideOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowLikedByMePosts = () => {
    setShowLikedByMePosts(!showLikedByMePosts);
    postsFilter.from = 0;
    postsFilter.showLikedByUserId = showLikedByMePosts ? undefined : userId;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const shareCopyPost = id => setSharedCopyPostId(id);
  const shareMailPost = id => setSharedMailPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="show only my posts"
          checked={showOwnPosts}
          disabled={hideOwnPosts}
          onChange={toggleShowOwnPosts}

        />
        <Checkbox
          className={styles.marginLeft}
          toggle
          label="hide my posts"
          checked={hideOwnPosts}
          disabled={showOwnPosts}
          onChange={toggleHideOwnPosts}
        />
        <Checkbox
          className={styles.marginLeft}
          toggle
          label="liked by me"
          checked={showLikedByMePosts}
          onChange={toggleShowLikedByMePosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            onPostEdit={handlePostEdit}
            onPostRemove={handlePostDelete}
            shareCopyPost={shareCopyPost}
            shareMailPost={shareMailPost}
            onLoadLikedUsers={handleLoadLikedUsers}
            onLoadDislikedUsers={handleLoadDislikedUsers}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost
            && (
              <ExpandedPost
                shareCopyPost={shareCopyPost}
                shareMailPost={shareMailPost}
                editPost={handlePostEdit}
              />
            )}
      {sharedCopyPostId && (
        <SharedPostLink
          postId={sharedCopyPostId}
          close={() => setSharedCopyPostId(undefined)}
        />
      )}
      {sharedMailPostId && (
        <SharedPostMail
          postId={sharedMailPostId}
          close={() => setSharedMailPostId(undefined)}
          onShareByMail={handleSharePostByEmail}
        />
      )}
    </div>
  );
};

export default Thread;
