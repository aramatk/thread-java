import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import { Comment as CommentUI, Modal, Post, Spinner } from 'src/components/common/common';
import AddComment from '../add-comment/add-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';

const ExpandedPost = ({
  shareCopyPost, shareMailPost, editPost
}) => {
  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.expandedPost
  }));
  const handleCommentEdit = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.editComment(commentPayload))
  ), [dispatch]);
  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.reactPost(id, true))
  ), [dispatch]);

  const handlePostDislike = React.useCallback(id => (
    dispatch(threadActionCreator.reactPost(id, false))
  ), [dispatch]);

  const handleCommentLike = React.useCallback(id => (
    dispatch(threadActionCreator.reactComment(id, true))
  ), [dispatch]);

  const handleCommentDislike = React.useCallback(id => (
    dispatch(threadActionCreator.reactComment(id, false))
  ), [dispatch]);

  const handlePostDelete = React.useCallback(id => (
    dispatch(threadActionCreator.deletePost(id))
  ), [dispatch]);

  const handleCommentDelete = React.useCallback(id => (
    dispatch(threadActionCreator.deleteComment(id))
  ), [dispatch]);

  const handleCommentAdd = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.addComment(commentPayload))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handleLoadCommentLikedUsers = React.useCallback(id => (
    dispatch(threadActionCreator.loadUsersCommentReactions(id, true))
  ), [dispatch]);

  const handleLoadCommentDislikedUsers = React.useCallback(id => (
    dispatch(threadActionCreator.loadUsersCommentReactions(id, false))
  ), [dispatch]);

  const handleLoadPostLikedUsers = React.useCallback(id => (
    dispatch(threadActionCreator.loadUsersPostReactions(id, true))
  ), [dispatch]);

  const handleLoadPostDislikedUsers = React.useCallback(id => (
    dispatch(threadActionCreator.loadUsersPostReactions(id, false))
  ), [dispatch]);

  const handleExpandedPostClose = () => handleExpandedPostToggle();

  const sortedComments = getSortedComments(post.comments ?? []);

  return (
    <Modal
      centered={false}
      open
      onClose={handleExpandedPostClose}
    >
      {post ? (
        <Modal.Content>
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            onPostEdit={editPost}
            onPostRemove={handlePostDelete}
            shareCopyPost={shareCopyPost}
            shareMailPost={shareMailPost}
            onLoadLikedUsers={handleLoadPostLikedUsers}
            onLoadDislikedUsers={handleLoadPostDislikedUsers}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <h3>Comments</h3>
            {sortedComments.map(comment => (
              <div key={comment.id}>
                <Comment
                  comment={comment}
                  onCommentEdit={handleCommentEdit}
                  onCommentRemove={handleCommentDelete}
                  onCommentLike={handleCommentLike}
                  onCommentDislike={handleCommentDislike}
                  onLoadDislikedUsers={handleLoadCommentDislikedUsers}
                  onLoadLikedUsers={handleLoadCommentLikedUsers}
                />
              </div>
            ))}
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
          </CommentUI.Group>
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  shareCopyPost: PropTypes.func.isRequired,
  shareMailPost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired
};

export default ExpandedPost;
