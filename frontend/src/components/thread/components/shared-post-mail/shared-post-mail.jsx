import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonColor, ButtonSize, ButtonType } from 'src/common/enums/enums';
import { Button, Form, Modal, Segment } from 'src/components/common/common';
import validator from 'validator';
import { useSelector } from 'react-redux';
import styles from './styles.module.scss';

const SharedPostMail = ({ postId, close, onShareByMail }) => {
  const [email, setEmail] = React.useState('');
  const [isEmailValid, setIsEmailValid] = React.useState(true);

  const { username } = useSelector(state => ({
    username: state.profile.user.username
  }));

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const handleMailClick = () => {
    const isValid = isEmailValid;
    if (!isValid) {
      return;
    }
    const sendEmailRequest = {
      recipients: [email],
      subject: 'Look at the post',
      body: `Hello!<br>${username} shared a post with you.<br>Have a look: ${window.location.origin}/share/${postId}`
    };
    onShareByMail(sendEmailRequest);
    close();
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
      </Modal.Header>
      <Modal.Content>
        <Form size="large" onSubmit={handleMailClick}>
          <Segment>
            <Form.Input
              fluid
              icon="at"
              iconPosition="left"
              placeholder="Email"
              type="email"
              error={!isEmailValid}
              onChange={ev => emailChanged(ev.target.value)}
              onBlur={() => setIsEmailValid(validator.isEmail(email))}
            />
            <Button
              type={ButtonType.SUBMIT}
              color={ButtonColor.TEAL}
              size={ButtonSize.LARGE}
              isFluid
              isPrimary
            >
              Send
            </Button>
          </Segment>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

SharedPostMail.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  onShareByMail: PropTypes.func.isRequired
};

export default SharedPostMail;
