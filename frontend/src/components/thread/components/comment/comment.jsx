import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import {
  Comment as CommentUI,
  EditRemoveMenu,
  EditTextModal,
  Icon,
  Label,
  UsersPopup
} from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { IconName } from 'src/common/enums/enums';
import styles from './styles.module.scss';

const Comment = ({
  comment, onCommentEdit, onCommentRemove, onCommentLike, onCommentDislike, onLoadLikedUsers, onLoadDislikedUsers
}) => {
  const {
    id,
    body,
    createdAt,
    user,
    likeCount,
    dislikeCount
  } = comment;
  const [editComment, setEditComment] = React.useState(undefined);
  const { userId } = useSelector(state => ({
    userId: state.profile.user.id
  }));

  const handleCommentEdit = txt => onCommentEdit({ id, body: txt });
  const handleCommentRemove = () => onCommentRemove(id);
  const handleLoadLikedUsers = () => onLoadLikedUsers(id);
  const handleLoadDislikedUsers = () => onLoadDislikedUsers(id);
  const handleCommentLike = () => onCommentLike(id);
  const handleCommentDislike = () => onCommentDislike(id);
  const showEditCommentModal = cmt => setEditComment(cmt);

  return (
    <>
      <CommentUI className={styles.comment}>
        <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
        <CommentUI.Content>
          <CommentUI.Author as="a">{user.username}</CommentUI.Author>
          <CommentUI.Metadata>
            {getFromNowTime(createdAt)}
          </CommentUI.Metadata>
          <CommentUI.Metadata style={{ display: 'block', marginLeft: '0px' }}>
            {user.status}
          </CommentUI.Metadata>
          <CommentUI.Text>{body}</CommentUI.Text>
        </CommentUI.Content>
        <CommentUI.Actions>
          <UsersPopup
            trigger={(
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={handleCommentLike}
              >
                <Icon name={IconName.THUMBS_UP} />
                {likeCount}
              </Label>
            )}
            loadUsersFunc={handleLoadLikedUsers}
          />
          <UsersPopup
            trigger={(
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={handleCommentDislike}
              >
                <Icon name={IconName.THUMBS_DOWN} />
                {dislikeCount}
              </Label>
            )}
            loadUsersFunc={handleLoadDislikedUsers}
          />
          {(userId === user.id) && (
            <EditRemoveMenu
              onRemove={handleCommentRemove}
              onEdit={showEditCommentModal}
            />
          )}
        </CommentUI.Actions>
      </CommentUI>
      {editComment && (
        <EditTextModal
          id={id}
          text={body}
          onEdit={handleCommentEdit}
          onClose={() => setEditComment(undefined)}
        />
      )}
    </>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentRemove: PropTypes.func.isRequired,
  onCommentEdit: PropTypes.func.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onCommentDislike: PropTypes.func.isRequired,
  onLoadLikedUsers: PropTypes.func.isRequired,
  onLoadDislikedUsers: PropTypes.func.isRequired
};

export default Comment;
