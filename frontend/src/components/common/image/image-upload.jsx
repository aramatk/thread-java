import React, { useState } from 'react';
import { image as imageService } from 'src/services/services';
import PropTypes from 'prop-types';
import { Button, Form, Message, Modal } from 'src/components/common/common';
import { ButtonColor, ButtonSize, ButtonType, IconName } from 'src/common/enums/enums';
import ImageCropper from './image-cropper';
import styles from './styles.module.scss';

const ImageUpload = ({ onImageSaved, close }) => {
  const [blob, setBlob] = useState(null);
  const [inputImg, setInputImg] = useState('');
  const [error, setError] = React.useState(undefined);
  const [isUploading, setIsUploading] = React.useState(false);

  const uploadImage = file => imageService.uploadImage(file);

  const getBlob = croppedImage => {
    // pass blob up from the ImageCropper component
    setBlob(croppedImage);
  };

  const onInputChange = e => {
    // convert image file to base64 string
    const file = e.target.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', () => {
      setInputImg(reader.result);
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  };

  const handleSubmitImage = () => {
    setIsUploading(true);
    uploadImage(blob)
      .then(({ id: imageId, link: imageLink }) => {
        setError(undefined);
        onImageSaved({ id: imageId, link: imageLink });
        close();
      })
      .catch(() => {
        setError('Error during image uploading');
      }).finally(() => {
        setIsUploading(false);
      });
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Upload image</span>
      </Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleSubmitImage}>
          <Button
            color="teal"
            isLoading={isUploading}
            isDisabled={isUploading}
            iconName={IconName.IMAGE}
          >
            <label className={styles.btnImgLabel}>
              Select image
              <input
                name="image"
                type="file"
                accept="image/*"
                onChange={onInputChange}
                hidden
              />
            </label>
          </Button>
          <br />
          <br />
          {
            inputImg && (
              <ImageCropper
                getBlob={getBlob}
                inputImg={inputImg}
              />
            )
          }
          <Button
            type={ButtonType.SUBMIT}
            color={ButtonColor.TEAL}
            size={ButtonSize.LARGE}
            isPrimary
          >
            Submit
          </Button>
          {error && (
            <Message color="red">
              {error}
            </Message>
          )}
        </Form>
      </Modal.Content>
    </Modal>
  );
};

ImageUpload.propTypes = {
  onImageSaved: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default ImageUpload;
