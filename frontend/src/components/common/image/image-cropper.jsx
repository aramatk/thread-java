import React, { useState } from 'react';
import Cropper from 'react-easy-crop';
import PropTypes from 'prop-types';
import { getCroppedImg } from './cropImage';
import styles from './styles.module.scss';

const ImageCropper = ({ getBlob, inputImg }) => {
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(1);

  /* onCropComplete() will occur each time the user modifies the cropped area,
      which isn't ideal. A better implementation would be getting the blob
      only when the user hits the submit button, but this works for now  */
  const onCropComplete = async (_, croppedAreaPixels) => {
    const croppedImage = await getCroppedImg(
      inputImg,
      croppedAreaPixels
    );
    getBlob(croppedImage);
  };

  return (
  /* need to have a parent with `position: relative`
          to prevent cropper taking up whole page */
    <div className={styles.cropper}>
      <Cropper
        image={inputImg}
        crop={crop}
        zoom={zoom}
        aspect={1}
        onCropChange={setCrop}
        onCropComplete={onCropComplete}
        onZoomChange={setZoom}
      />
    </div>
  );
};

ImageCropper.propTypes = {
  getBlob: PropTypes.instanceOf(Object).isRequired,
  inputImg: PropTypes.string.isRequired
};

export default ImageCropper;
