import * as React from 'react';
import PropTypes from 'prop-types';

const LimitedTextarea = ({ value, limit, placeholder, onChange, style }) => {
  const format = txt => (txt ? txt.slice(0, limit) : '');

  const [content, setContent] = React.useState(format(value));

  const setFormattedContent = React.useCallback(
    text => {
      setContent(text.slice(0, limit));
      if (onChange) {
        onChange(text);
      }
    },
    [limit, setContent, onChange]
  );

  return (
    <>
      <textarea
        placeholder={placeholder}
        onChange={event => setFormattedContent(event.target.value)}
        value={content}
        style={style}
      />
      <p>
        {content.length}
        /
        {limit}
      </p>
    </>
  );
};

LimitedTextarea.defaultProps = {
  value: '',
  limit: null,
  onChange: null,
  placeholder: null,
  style: null
};

LimitedTextarea.propTypes = {
  value: PropTypes.string,
  limit: PropTypes.number,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
};

export default LimitedTextarea;
