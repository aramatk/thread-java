import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import {
  Card,
  EditRemoveMenu,
  Icon,
  Image,
  Label,
  UsersPopup,
  ShareMenuPopup,
  EditTextModal
} from 'src/components/common/common';
import { useSelector } from 'react-redux';
import styles from './styles.module.scss';

const Post = ({
  post, onPostLike, onPostDislike, onExpandedPostToggle, shareCopyPost, shareMailPost, onPostEdit, onPostRemove,
  onLoadLikedUsers, onLoadDislikedUsers
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handlePostEdit = txt => onPostEdit({ id, body: txt });
  const handlePostRemove = () => onPostRemove(id);
  const handleLoadLikedUsers = () => onLoadLikedUsers(id);
  const handleLoadDislikedUsers = () => onLoadDislikedUsers(id);
  const handleShareCopyPost = () => shareCopyPost(id);
  const handleShareMailPost = () => shareMailPost(id);

  const { userId } = useSelector(state => ({
    userId: state.profile.user.id
  }));
  const [isEditPost, setIsEditPost] = React.useState(false);

  const showEditPostModal = () => setIsEditPost(true);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>{body}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <UsersPopup
          trigger={(
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={handlePostLike}
            >
              <Icon name={IconName.THUMBS_UP} />
              {likeCount}
            </Label>
          )}
          loadUsersFunc={handleLoadLikedUsers}
        />

        <UsersPopup
          trigger={(
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={handlePostDislike}>
              <Icon name={IconName.THUMBS_DOWN} />
              {dislikeCount}
            </Label>
          )}
          loadUsersFunc={handleLoadDislikedUsers}
        />
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>

        <ShareMenuPopup
          trigger={(
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
            >
              <Icon name={IconName.SHARE_ALTERNATE} />
            </Label>
          )}
          onShareCopyPost={handleShareCopyPost}
          onShareMailPost={handleShareMailPost}
        />
        {(userId === user.id) && (
          <EditRemoveMenu
            onRemove={handlePostRemove}
            onEdit={showEditPostModal}
          />
        )}
        {isEditPost && (
          <EditTextModal
            text={body}
            onEdit={handlePostEdit}
            onClose={() => setIsEditPost(false)}
          />
        )}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  shareCopyPost: PropTypes.func.isRequired,
  shareMailPost: PropTypes.func.isRequired,
  onPostEdit: PropTypes.func.isRequired,
  onPostRemove: PropTypes.func.isRequired,
  onLoadLikedUsers: PropTypes.func.isRequired,
  onLoadDislikedUsers: PropTypes.func.isRequired
};

export default Post;
