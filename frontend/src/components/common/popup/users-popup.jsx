import * as React from 'react';
import PropTypes from 'prop-types';
import { Popup, List } from 'semantic-ui-react';
import { Spinner } from 'src/components/common/common';
import styles from './styles.module.scss';

const UsersPopup = ({ loadUsersFunc, trigger }) => {
  const [users, setUsers] = React.useState(null);

  const handleLoadUsers = async () => {
    const response = await loadUsersFunc();
    setUsers(response);
  };

  return (
    <Popup
      inverted
      className={styles.usersPopupStyle}
      on="hover"
      onClose={() => {
        setUsers(null);
      }}
      onOpen={handleLoadUsers}
      popperDependencies={[!!users]}
      trigger={trigger}
      wide
      open={users !== undefined && users !== null && users.length > 0}
    >
      {users === null || users === undefined ? (<Spinner />) : (
        <List>
          {users && users.map(user => (
            <List.Item key={user.id}>{user.username}</List.Item>
          ))}
        </List>
      )}
    </Popup>
  );
};

UsersPopup.propTypes = {
  loadUsersFunc: PropTypes.func.isRequired,
  trigger: PropTypes.node.isRequired
};

export default UsersPopup;
