import * as React from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Modal } from 'src/components/common/common';
import { ButtonType } from 'src/common/enums/enums';
import styles from '../../thread/components/add-post/styles.module.scss';

const EditTextModal = ({ text, onEdit, onClose }) => {
  const [body, setBody] = React.useState(text);

  const handleEdit = async () => {
    if (!body) {
      return;
    }
    await onEdit(body);
    onClose();
  };

  return (
    <Modal
      open
      onClose={onClose}
    >
      <Modal.Content>
        <Form reply onSubmit={handleEdit}>
          <Form.TextArea
            value={body}
            onChange={ev => setBody(ev.target.value)}
          />
          <div className={styles.btnWrapper}>
            <Button type={ButtonType.SUBMIT} isPrimary>
              Save
            </Button>
          </div>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

EditTextModal.propTypes = {
  text: PropTypes.string.isRequired,
  onEdit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export default EditTextModal;
