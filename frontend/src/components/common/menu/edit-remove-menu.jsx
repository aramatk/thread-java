import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Label } from 'src/components/common/common';
import { IconName } from 'src/common/enums/enums';
import styles from './styles.module.scss';

const EditRemoveMenu = ({ onEdit, onRemove }) => (
  <span className="edit-remove-menu">
    <Label
      basic
      size="small"
      as="a"
      className={styles.menubarBtn}
      onClick={onEdit}
    >
      <Icon name={IconName.EDIT} />
    </Label>
    <Label
      basic
      size="small"
      as="a"
      className={styles.menubarBtn}
      onClick={onRemove}
    >
      <Icon name={IconName.REMOVE} />
    </Label>
  </span>
);

EditRemoveMenu.propTypes = {
  onEdit: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired
};

export default EditRemoveMenu;
