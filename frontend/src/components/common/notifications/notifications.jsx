import * as React from 'react';
import PropTypes from 'prop-types';
import { Stomp } from '@stomp/stompjs';
import SockJS from 'sockjs-client';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { userType } from 'src/common/prop-types/prop-types';

import 'react-notifications/lib/notifications.css';

const Notifications = ({
  user, onPostApply, onPostUpdate, onPostRemove, onCommentApply,
  onCommentUpdate, onCommentRemove, onPostReactionUpdate, onCommentReactionUpdate
}) => {
  const [stompClient] = React.useState(Stomp.over(new SockJS('/ws')));
  React.useEffect(() => {
    if (!user) {
      return undefined;
    }

    stompClient.debug = () => {
    };
    stompClient.connect({}, () => {
      const { id } = user;

      stompClient.subscribe(`/user/${id}/like`, () => {
        NotificationManager.info('Your post was liked!');
      });

      stompClient.subscribe(`/user/${id}/comment/like`, () => {
        NotificationManager.info('Your comment was liked!');
      });

      stompClient.subscribe(`/user/${id}/post/comment`, () => {
        NotificationManager.info('Your post was commented!');
      });

      stompClient.subscribe('/topic/post/new', message => {
        const post = JSON.parse(message.body);
        if (post.userId !== id) {
          onPostApply(post.id);
        }
      });

      stompClient.subscribe('/topic/post/update', message => {
        const post = JSON.parse(message.body);
        if (post.userId !== id) {
          onPostUpdate(post.id);
        }
      });

      stompClient.subscribe('/topic/post/remove', message => {
        const post = JSON.parse(message.body);
        if (post.userId !== id) {
          onPostRemove(post.id);
        }
      });

      stompClient.subscribe('/topic/post/react', message => {
        const notification = JSON.parse(message.body);
        if (notification?.userId !== id) {
          onPostReactionUpdate(notification.reaction, notification.postId, notification.isLike);
        }
      });

      stompClient.subscribe('/topic/comment/update', message => {
        const comment = JSON.parse(message.body);
        if (comment.userId !== id) {
          onCommentUpdate(comment.id);
        }
      });

      stompClient.subscribe('/topic/comment/remove', message => {
        const comment = JSON.parse(message.body);
        if (comment.userId !== id) {
          onCommentRemove(comment.id);
        }
      });

      stompClient.subscribe('/topic/comment/new', message => {
        const comment = JSON.parse(message.body);
        if (comment.userId !== id) {
          onCommentApply(comment.id);
        }
      });

      stompClient.subscribe('/topic/comment/react', message => {
        const notification = JSON.parse(message.body);
        if (notification.userId !== id) {
          onCommentReactionUpdate(notification.reaction, notification.commentId, notification.isLike);
        }
      });
    });

    return () => {
      stompClient.onDisconnect(() => {
      });
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: userType,
  onPostApply: PropTypes.func.isRequired,
  onPostUpdate: PropTypes.func.isRequired,
  onPostRemove: PropTypes.func.isRequired,
  onCommentApply: PropTypes.func.isRequired,
  onCommentUpdate: PropTypes.func.isRequired,
  onCommentRemove: PropTypes.func.isRequired,
  onPostReactionUpdate: PropTypes.func.isRequired,
  onCommentReactionUpdate: PropTypes.func.isRequired
};

export default Notifications;
