import React, { useState } from 'react';
import { Menu, Popup } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { Icon } from 'src/components/common/common';
import { IconName } from 'src/common/enums/enums';

const ShareMenuPopup = ({ onShareCopyPost, onShareMailPost, trigger }) => {
  const [activeItem, setActiveItem] = useState();
  const [isOpen, setOpen] = useState(false);

  const handleShareCopyPost = (e, { name }) => {
    setActiveItem(name);
    setOpen(false);
    onShareCopyPost();
  };

  const handleShareMailPost = (e, { name }) => {
    setActiveItem(name);
    setOpen(false);
    onShareMailPost();
  };

  const handleOpenPopup = () => {
    setOpen(true);
  };

  const handleClosePopup = () => {
    setOpen(false);
  };

  const style = {
    borderRadius: 0,
    border: '0px',
    boxShadow: 'none',
    padding: '0px'
  };

  return (
    <Popup
      basic
      on="click"
      style={style}
      open={isOpen}
      onOpen={handleOpenPopup}
      onClose={handleClosePopup}
      trigger={trigger}
    >
      <Menu compact vertical size="mini">
        <Menu.Item
          name="copy"
          active={activeItem === 'copy'}
          onClick={handleShareCopyPost}
        >
          <Icon name={IconName.COPY} />
          copy
        </Menu.Item>

        <Menu.Item
          name="mail"
          active={activeItem === 'mail'}
          onClick={handleShareMailPost}
        >
          <Icon name={IconName.MAIL} />
          mail
        </Menu.Item>
      </Menu>
    </Popup>
  );
};

ShareMenuPopup.propTypes = {
  onShareCopyPost: PropTypes.func.isRequired,
  onShareMailPost: PropTypes.func.isRequired,
  trigger: PropTypes.node.isRequired
};

export default ShareMenuPopup;
