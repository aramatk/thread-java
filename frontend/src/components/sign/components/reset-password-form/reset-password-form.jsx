import * as React from 'react';
import PropTypes from 'prop-types';
import { AppRoute, ButtonColor, ButtonSize, ButtonType } from 'src/common/enums/enums';
import { Button, Form, Message, Modal, Segment } from 'src/components/common/common';
import validator from 'validator';
import { Redirect } from 'react-router-dom';
import { NotificationManager } from 'react-notifications';
import styles from './styles.module.scss';

const ResetPasswordForm = ({ onResetPassword }) => {
  const [email, setEmail] = React.useState('');
  const [error, setError] = React.useState(undefined);
  const [redirectToRoot, setRedirectToRoot] = React.useState(false);
  const [isEmailValid, setIsEmailValid] = React.useState(true);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const resetPassword = async () => {
    const isValid = isEmailValid;
    if (!isValid) {
      return;
    }
    const response = await onResetPassword(email);
    setError(response.error);
    if (!response.error) {
      setRedirectToRoot(true);
      NotificationManager.info('Reset password link was sent to your email!');
    }
  };

  return (
    <Modal open>
      <Modal.Header className={styles.header}>
        <span>Specify your email</span>
      </Modal.Header>
      <Modal.Content>
        <Form size="large" onSubmit={resetPassword}>
          <Segment>
            <Form.Input
              fluid
              icon="at"
              iconPosition="left"
              placeholder="Email"
              type="email"
              error={!isEmailValid}
              onChange={ev => emailChanged(ev.target.value)}
              onBlur={() => setIsEmailValid(validator.isEmail(email))}
            />
            <Button
              type={ButtonType.SUBMIT}
              color={ButtonColor.TEAL}
              size={ButtonSize.LARGE}
              isFluid
              isPrimary
            >
              Reset
            </Button>
          </Segment>
        </Form>
        {error && (
          <Message color="red">
            {error}
          </Message>
        )}
        {redirectToRoot && (
          <Redirect to={AppRoute.ROOT} />
        )}
      </Modal.Content>
    </Modal>
  );
};

ResetPasswordForm.propTypes = {
  onResetPassword: PropTypes.func.isRequired
};

export default ResetPasswordForm;
