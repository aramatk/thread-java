import LoginForm from './login-form/login-form';
import RegistrationForm from './registration-form/registration-form';
import ResetPasswordForm from './reset-password-form/reset-password-form';

export { LoginForm, RegistrationForm, ResetPasswordForm };
