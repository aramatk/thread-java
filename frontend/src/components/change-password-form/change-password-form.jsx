import * as React from 'react';
import { AppRoute, ButtonColor, ButtonSize, ButtonType } from 'src/common/enums/enums';
import { Button, Form, Segment } from 'src/components/common/common';
import { useDispatch } from 'react-redux';
import { profileActionCreator } from 'src/store/actions';
import { routeMatchType } from 'src/common/prop-types/prop-types';
import { NotificationManager } from 'react-notifications';
import { Redirect } from 'react-router-dom';
import styles from './styles.module.scss';
import { Grid, Image, Message } from '../common/common';

const ChangePasswordForm = ({ match }) => {
  const [userId, setUserId] = React.useState(undefined);
  const [password, setPassword] = React.useState('');
  const [repeatPassword, setRepeatPassword] = React.useState('');
  const [isPasswordValid, setIsPasswordValid] = React.useState(true);
  const [isRepeatPasswordValid, setIsRepeatPasswordValid] = React.useState(true);
  const [error, setError] = React.useState(undefined);
  const [redirectToRoot, setRedirectToRoot] = React.useState(false);

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const passwordRepeatChanged = data => {
    setRepeatPassword(data);
    setIsRepeatPasswordValid(true);
  };

  const dispatch = useDispatch();

  const handleChangePassword = React.useCallback(
    (id, newPassword) => dispatch(
      profileActionCreator.changeUserPassword({ userId: id, password: newPassword })
    ),
    [dispatch]
  );

  const handleValidateToken = React.useCallback(
    token => dispatch(profileActionCreator.validateToken(token)),
    [dispatch]
  );

  React.useEffect(() => {
    async function validateToken() {
      const response = await handleValidateToken(match.params.token);
      setError(response.error);
      setUserId(response.userId);
    }

    validateToken();
  }, [match.params.token, handleValidateToken]);

  const handleChangePasswordClick = async () => {
    const isValid = isPasswordValid && isRepeatPasswordValid && password === repeatPassword;
    if (password !== repeatPassword) {
      setError('Passwords must be equal');
    }
    if (!isValid) {
      return;
    }
    const response = await handleChangePassword(userId, password);
    setError(response.error);
    if (!response.error) {
      setRedirectToRoot(true);
      NotificationManager.info(response.message);
      setUserId(undefined);
    }
  };

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <h2 className={styles.wrapper}>
          <Image
            width="75"
            height="75"
            circular
            src="http://s1.iconbird.com/ico/2013/8/428/w256h2561377930292cattied.png"
          />
          Thread
        </h2>
        {userId && (
          <>
            <h2 className={styles.title}>Change password</h2>
            <Form name="changePasswordForm" size="large" onSubmit={handleChangePasswordClick}>
              <Segment>
                <Form.Input
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Password"
                  type="password"
                  error={!isPasswordValid}
                  onChange={ev => passwordChanged(ev.target.value)}
                  onBlur={() => setIsPasswordValid(Boolean(password))}
                />
                <Form.Input
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Repeat password"
                  type="password"
                  error={!isRepeatPasswordValid}
                  onChange={ev => passwordRepeatChanged(ev.target.value)}
                  onBlur={() => setIsRepeatPasswordValid(Boolean(repeatPassword))}
                />
                <Button
                  type={ButtonType.SUBMIT}
                  color={ButtonColor.TEAL}
                  size={ButtonSize.LARGE}
                  isFluid
                  isPrimary
                >
                  Save
                </Button>
              </Segment>
            </Form>
          </>
        )}
        {error && (
          <Message color="red">
            {error}
          </Message>
        )}
        {redirectToRoot && (
          <Redirect to={AppRoute.ROOT} />
        )}
      </Grid.Column>
    </Grid>
  );
};

ChangePasswordForm.propTypes = {
  match: routeMatchType.isRequired
};

export default ChangePasswordForm;
