import { ContentType, HttpMethod } from 'src/common/enums/enums';

class Mail {
  constructor({ http }) {
    this._http = http;
  }

  sendEmail(payload) {
    return this._http.load('/api/mail/send', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }
}

export { Mail };
