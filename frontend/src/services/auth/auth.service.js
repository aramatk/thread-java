import { ContentType, HttpMethod } from 'src/common/enums/enums';

class Auth {
  constructor({ http }) {
    this._http = http;
  }

  login(payload) {
    return this._http.load('/api/auth/login', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  registration(payload) {
    return this._http.load('/api/auth/register', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  changeUserPassword(payload) {
    return this._http.load('/api/reset/password', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  resetUserPassword(email) {
    const formData = new FormData();
    formData.append('email', email);
    return this._http.load('/api/reset/password', {
      method: HttpMethod.POST,
      hasAuth: false,
      payload: formData
    });
  }

  validateToken(token) {
    return this._http.load(`/api/reset/password/${token}`, {
      method: HttpMethod.GET,
      contentType: ContentType.JSON,
      hasAuth: false
    });
  }
}

export { Auth };
