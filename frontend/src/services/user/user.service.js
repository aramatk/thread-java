import { HttpMethod } from 'src/common/enums/enums';

class User {
  constructor({ http }) {
    this._http = http;
  }

  getCurrentUser() {
    return this._http.load('/api/user', {
      method: HttpMethod.GET
    });
  }

  updateUser(user) {
    return this._http.load('/api/user', {
      method: HttpMethod.PUT,
      query: user
    });
  }
}

export { User };
