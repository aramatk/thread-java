import { createAction } from '@reduxjs/toolkit';
import { StorageKey } from 'src/common/enums/enums';
import {
  storage as storageService,
  auth as authService,
  user as userService
} from 'src/services/services';

const ActionType = {
  SET_USER: 'profile/set-user'
};

const setUser = createAction(ActionType.SET_USER, user => ({
  payload: {
    user
  }
}));

const login = request => async dispatch => {
  const { user, token } = await authService.login(request);

  storageService.setItem(StorageKey.TOKEN, token);
  dispatch(setUser(user));
};

const register = request => async dispatch => {
  const { user, token } = await authService.registration(request);

  storageService.setItem(StorageKey.TOKEN, token);
  dispatch(setUser(user));
};

const logout = () => dispatch => {
  storageService.removeItem(StorageKey.TOKEN);
  dispatch(setUser(null));
};

const loadCurrentUser = () => async dispatch => {
  const user = await userService.getCurrentUser();

  dispatch(setUser(user));
};

const changeUserPassword = request => async () => {
  const message = await authService.changeUserPassword(request);
  return message;
};

const resetUserPassword = email => async () => {
  const message = await authService.resetUserPassword(email);
  return message;
};

const validateToken = token => async () => {
  const response = await authService.validateToken(token);
  return response;
};

const updateUser = user => async () => {
  const response = await userService.updateUser(user);
  return response;
};

export { setUser, resetUserPassword, changeUserPassword, validateToken, login, register, logout, loadCurrentUser,
  updateUser };
