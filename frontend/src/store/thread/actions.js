import { createAction } from '@reduxjs/toolkit';
import { comment as commentService, mail as mailService, post as postService } from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const updateEditedPostPr = async (id, dispatch, getRootState) => {
  const newPost = await postService.getPost(id);
  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const updatedPosts = posts.map(p => (p.id !== id ? p : newPost));
  dispatch(setPosts(updatedPosts));

  if (expandedPost && expandedPost.id === id) {
    dispatch(setExpandedPost(newPost));
  }
};

const updateEditedPost = postId => async (dispatch, getRootState) => {
  updateEditedPostPr(postId, dispatch, getRootState);
};

const editPost = post => async (dispatch, getRootState) => {
  const id = await postService.updatePost(post);
  if (id === undefined || id === null) {
    return;
  }
  await updateEditedPostPr(id, dispatch, getRootState);
};

const updateRemovedPostPr = (postId, dispatch, getRootState) => {
  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const updatedPosts = posts.filter(p => p.id !== postId);
  dispatch(setPosts(updatedPosts));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(null));
  }
};

const updateRemovedPost = postId => async (dispatch, getRootState) => {
  updateRemovedPostPr(postId, dispatch, getRootState);
};

const deletePost = postId => async (dispatch, getRootState) => {
  const id = postId ? await postService.deletePost(postId) : undefined;

  if (id === undefined || id === null) {
    return;
  }
  updateRemovedPostPr(id, dispatch, getRootState);
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const newLikeCount = (likeCount, isLike, diff, changedDiff) => {
  if (isLike) {
    return likeCount + diff;
  }
  return likeCount + changedDiff;
};

const newDislikeCount = (dislikeCount, isLike, diff, changedDiff) => {
  if (!isLike) {
    return dislikeCount + diff;
  }
  return dislikeCount + changedDiff;
};

const mapLikes = (obj, isLike, diff, changedDiff) => ({
  ...obj,
  likeCount: newLikeCount(Number(obj.likeCount), isLike, diff, changedDiff),
  dislikeCount: newDislikeCount(Number(obj.dislikeCount), isLike, diff, changedDiff)
});

const updateReactPostPr = (reaction, postId, isLike, dispatch, getRootState) => {
  // if ID exists then the reaction post was created, otherwise - reaction post was removed
  const diff = reaction?.id ? 1 : -1;
  // if reactionChanged true then the reaction post was changed
  const changedDiff = reaction?.reactionChanged ? -1 : 0;

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post, isLike, diff, changedDiff)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost, isLike, diff, changedDiff)));
  }
};

const updateReactPost = (reaction, postId, isLike) => async (dispatch, getRootState) => {
  updateReactPostPr(reaction, postId, isLike, dispatch, getRootState);
};

const reactPost = (postId, isLike) => async (dispatch, getRootState) => {
  const response = await postService.reactPost(postId, isLike);
  updateReactPostPr(response, postId, isLike, dispatch, getRootState);
};

const applyCommentPr = async (commentId, dispatch, getRootState) => {
  const comment = await commentService.getComment(commentId);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const applyComment = commentId => async (dispatch, getRootState) => {
  applyCommentPr(commentId, dispatch, getRootState);
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  await applyCommentPr(id, dispatch, getRootState);
};

const updateComment = (commentId, postId, updateFnc, countDiff, dispatch, getRootState) => {
  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + countDiff,
    comments: updateFnc([...(post.comments || [])])
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const updatedPosts = posts.map(post => (post.id !== postId ? post : mapComments(post)));
  dispatch(setPosts(updatedPosts));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const updateEditedCommentPr = async (commentId, dispatch, getRootState) => {
  const newComment = await commentService.getComment(commentId);
  const changeComment = comments => comments.map(с => (с.id !== commentId ? с : newComment));
  updateComment(commentId, newComment.postId, changeComment, 0, dispatch, getRootState);
};

const editComment = comment => async (dispatch, getRootState) => {
  const commentId = await commentService.updateComment(comment);
  if (commentId === undefined || commentId === null) {
    return;
  }
  await updateEditedCommentPr(commentId, dispatch, getRootState);
};

const updateEditedComment = commentId => async (dispatch, getRootState) => {
  updateEditedCommentPr(commentId, dispatch, getRootState);
};

const updateRemovedCommentPr = async (commentId, dispatch, getRootState) => {
  const comment = await commentService.getComment(commentId);
  const removeComment = comments => comments.filter(с => с.id !== commentId);
  updateComment(commentId, comment.postId, removeComment, -1, dispatch, getRootState);
};

const updateRemovedComment = commentId => async (dispatch, getRootState) => {
  updateRemovedCommentPr(commentId, dispatch, getRootState);
};

const deleteComment = id => async (dispatch, getRootState) => {
  const commentId = id ? await commentService.deleteComment(id) : undefined;
  if (commentId === undefined || commentId === null) {
    return;
  }
  await updateRemovedCommentPr(commentId, dispatch, getRootState);
};

const updateCommentReactionPr = (reaction, commentId, isLike, dispatch, getRootState) => {
  // if ID exists then the reaction comment was created, otherwise - reaction comment was removed
  const diff = reaction?.id ? 1 : -1;
  // if reactionChanged true then the reaction comment was changed
  const changedDiff = reaction?.reactionChanged ? -1 : 0;
  const {
    posts: { expandedPost }
  } = getRootState();

  if (expandedPost) {
    const comment = expandedPost.comments.find(c => c.id === commentId);
    if (comment) {
      const updatedComment = mapLikes(comment, isLike, diff, changedDiff);
      const update = comments => comments.map(c => (c.id === commentId ? updatedComment : c));
      const mapComments = post => ({
        ...post,
        comments: update([...(post.comments || [])])
      });
      dispatch(setExpandedPost(mapComments(expandedPost)));
    }
  }
};

const updateReactComment = (reaction, commentId, isLike) => async (dispatch, getRootState) => {
  updateCommentReactionPr(reaction, commentId, isLike, dispatch, getRootState);
};

const reactComment = (commentId, isLike) => async (dispatch, getRootState) => {
  const response = await commentService.reactComment(commentId, isLike);
  updateCommentReactionPr(response, commentId, isLike, dispatch, getRootState);
};

const loadUsersPostReactions = (postId, isLike) => async () => {
  const users = (postId && (isLike !== undefined || isLike !== null)
    ? await postService.getUsersPostReaction(postId, isLike) : undefined);
  return users;
};

const loadUsersCommentReactions = (commentId, isLike) => async () => {
  const users = (commentId && (isLike !== undefined || isLike !== null)
    ? await commentService.getUsersCommentReaction(commentId, isLike) : undefined);
  return users;
};

const sendEmail = sendEmailRequest => async () => {
  mailService.sendEmail(sendEmailRequest);
};

export {
  setPosts,
  addMorePosts,
  addPost,
  editPost,
  deletePost,
  setExpandedPost,
  loadPosts,
  loadMorePosts,
  createPost,
  toggleExpandedPost,
  reactPost,
  addComment,
  editComment,
  deleteComment,
  reactComment,
  loadUsersPostReactions,
  loadUsersCommentReactions,
  sendEmail,
  applyPost,
  updateEditedPost,
  updateRemovedPost,
  applyComment,
  updateEditedComment,
  updateRemovedComment,
  updateReactPost,
  updateReactComment
};
