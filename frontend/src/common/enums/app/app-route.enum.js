const AppRoute = {
  ROOT: '/',
  ANY: '*',
  LOGIN: '/login',
  REGISTRATION: '/registration',
  PROFILE: '/profile',
  SHARE_$POSTHASH: '/share/:postHash',
  RESET_PASSWORD: '/reset/password',
  RESET_PASSWORD_$TOKEN: '/reset/password/:token'
};

export { AppRoute };
