package com.threadjava.email;

import com.threadjava.email.dto.SendMailRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/mail")
public class MailController {
    @Autowired
    public IMailService mailService;

    @PostMapping("/send")
    public ResponseEntity<String> sendEmail(@RequestBody SendMailRequest sendMailRequest) {
        mailService.sendEmail(sendMailRequest);
        return new ResponseEntity<>("OK!!!", HttpStatus.NO_CONTENT);
    }
}