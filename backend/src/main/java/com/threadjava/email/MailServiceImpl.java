package com.threadjava.email;

import com.threadjava.email.dto.SendMailRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailServiceImpl implements IMailService {
    @Autowired
    public JavaMailSender emailSender;

    private static final Logger log = LoggerFactory.getLogger(MailServiceImpl.class);

    @Override
    @Async
    public void sendEmail(SendMailRequest sendMailRequest) {
        log.info("Send email {}", sendMailRequest);
        MimeMessage message = emailSender.createMimeMessage();
        boolean multipart = true;
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");
            message.setContent(sendMailRequest.getBody(), "text/html");
            helper.setTo(sendMailRequest.getRecipients().toArray(new String[0]));
            helper.setSubject(sendMailRequest.getSubject());
            this.emailSender.send(message);
        } catch (MessagingException e) {
            log.info("Exception occurred during sending email {}", e.getMessage());
        }
        log.info("Email was successfully sent!");
    }
}
