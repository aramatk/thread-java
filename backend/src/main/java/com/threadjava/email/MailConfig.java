package com.threadjava.email;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class MailConfig {
    @Value(value = "${mail.sender.username}")
    private String USERNAME;
    @Value(value = "${mail.sender.password}")
    private String PASSWORD;
    @Value(value = "${mail.host}")
    private String HOST;
    @Value(value = "${mail.port}")
    private Integer PORT;
    @Value(value = "${mail.transport.protocol}")
    private String TRANSPORT_PROTOCOL;
    @Value(value = "${mail.smtp.auth}")
    private Boolean AUTH;
    @Value(value = "${mail.smtp.starttls.enable}")
    private Boolean STARTTLS_ENABLE;
    @Value(value = "${mail.debug}")
    private Boolean DEBUG;
    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(HOST);
        mailSender.setPort(PORT);

        mailSender.setUsername(USERNAME);
        mailSender.setPassword(PASSWORD);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", TRANSPORT_PROTOCOL);
        props.put("mail.smtp.auth",AUTH);
        props.put("mail.smtp.starttls.enable", STARTTLS_ENABLE);
        props.put("mail.debug", DEBUG);

        return mailSender;
    }
}
