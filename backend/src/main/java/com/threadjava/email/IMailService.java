package com.threadjava.email;

import com.threadjava.email.dto.SendMailRequest;

public interface IMailService {
    void sendEmail(SendMailRequest sendMailRequest);
}
