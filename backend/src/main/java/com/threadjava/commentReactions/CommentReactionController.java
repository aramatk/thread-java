package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.NotificationCommentReactionDto;
import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import com.threadjava.users.UserMapper;
import com.threadjava.users.dto.UserDetailsDto;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/commentreaction")
public class CommentReactionController {
    @Autowired
    private CommentReactionService commentReactionService;
    @Autowired
    private CommentReactionsRepository commentReactionsRepository;
    @Autowired
    private SimpMessagingTemplate template;

    @PutMapping
    public Optional<ResponseCommentReactionDto> setReaction(@RequestBody ReceivedCommentReactionDto commentReactionDto) {
        commentReactionDto.setUserId(getUserId());
        var reaction = commentReactionService.setReaction(commentReactionDto);
        if (reaction.isPresent() && !getUserId().equals(reaction.get().getAuthorId())
                && Boolean.TRUE.equals(reaction.get().getIsLike())) {
            // notify a user if someone (not himself) liked his comment
            template.convertAndSendToUser(
                    reaction.get().getAuthorId().toString(),
                    "/comment/like",
                    "Your comment was liked!"
            );
        }
        template.convertAndSend("/topic/comment/react",  NotificationCommentReactionDto.builder()
                .userId(getUserId())
                .commentId(commentReactionDto.getCommentId())
                .isLike(commentReactionDto.getIsLike())
                .reaction(reaction)
                .build());
        return reaction;
    }

    @GetMapping("/{commentId}/{isLike}")
    public List<UserDetailsDto> getUsersWithReaction(@PathVariable UUID commentId, @PathVariable Boolean isLike) {
        Validate.notNull(commentId, "Comment id must not be null!");
        Validate.notNull(isLike, "isLike must not be null!");

        return commentReactionsRepository.getUsersWithReactions(commentId, isLike).stream()
                .map(u -> UserMapper.MAPPER.userToUserDetailsDto(u))
                .collect(Collectors.toList());
    }
}
