package com.threadjava.commentReactions;

import com.threadjava.comment.CommentService;
import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import com.threadjava.commentReactions.model.CommentReaction;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentReactionService {
    @Autowired
    private CommentReactionsRepository commentReactionsRepository;

    @Autowired
    private CommentService commentService;

    public Optional<ResponseCommentReactionDto> setReaction(ReceivedCommentReactionDto commentReactionDto) {

        var reaction = commentReactionsRepository.getCommentReaction(commentReactionDto.getUserId(), commentReactionDto.getCommentId());

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike() == commentReactionDto.getIsLike()) {
                commentReactionsRepository.deleteById(react.getId());
                return Optional.empty();
            } else {
                react.setIsLike(commentReactionDto.getIsLike());
                Optional<ResponseCommentReactionDto> responseCommentReactionDto = save(react);
                responseCommentReactionDto.get().setReactionChanged(true);
                return responseCommentReactionDto;
            }
        } else {
            var commentReaction = CommentReactionMapper.MAPPER.dtoToPostReaction(commentReactionDto);
            return save(commentReaction);
        }
    }

    private Optional<ResponseCommentReactionDto> save(CommentReaction newReaction) {
        var reaction = commentReactionsRepository.save(newReaction);
        var comment = commentService.getCommentById(reaction.getComment().getId());
        return Optional.of(
                ResponseCommentReactionDto
                        .builder()
                        .id(reaction.getId())
                        .isLike(reaction.getIsLike())
                        .userId(reaction.getUser().getId())
                        .commentId(comment.getId())
                        .authorId(comment.getUser().getId())
                        .reactionChanged(false)
                        .build()
        );
    }
}
