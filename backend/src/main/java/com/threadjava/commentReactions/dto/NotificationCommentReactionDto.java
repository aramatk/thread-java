package com.threadjava.commentReactions.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;
import java.util.UUID;

@Data
@Builder
public class NotificationCommentReactionDto {
    private UUID userId;
    private UUID commentId;
    private Boolean isLike;
    private Optional<ResponseCommentReactionDto> reaction;
}
