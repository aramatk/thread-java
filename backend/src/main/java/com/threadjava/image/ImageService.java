package com.threadjava.image;

import com.cloudinary.Cloudinary;
import com.cloudinary.Singleton;
import com.cloudinary.utils.ObjectUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.threadjava.image.dto.ImageDto;
import com.threadjava.image.dto.СloudinaryImage;
import com.threadjava.image.model.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class ImageService {
    @Autowired
    ImageRepository imageRepository;

    public ImageDto upload(MultipartFile file) throws IOException {
        Cloudinary cloudinary = Singleton.getCloudinary();
        var uploadResult = cloudinary.uploader().upload(file.getBytes(), ObjectUtils.emptyMap());
        var mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(uploadResult);
        СloudinaryImage cloudinaryImage = mapper.readValue(json, СloudinaryImage.class);
        var image = new Image();
        image.setLink(cloudinaryImage.getSecureUrl());
        image.setDeleteHash(cloudinaryImage.getPublicId());
        var imageEntity = imageRepository.save(image);
        return ImageMapper.MAPPER.imageToImageDto(imageEntity);
    }
}
