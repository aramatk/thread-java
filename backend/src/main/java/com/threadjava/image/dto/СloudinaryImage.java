package com.threadjava.image.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class СloudinaryImage {
    @JsonProperty("signature")
    private String signature;
    @JsonProperty("format")
    private String format;
    @JsonProperty("resource_type")
    private String resourceType;
    @JsonProperty("secure_url")
    private String secureUrl;
    @JsonProperty("created_at")
    private Date createdAt;
    @JsonProperty("asset_id")
    private String assetId;
    @JsonProperty("version_id")
    private String versionId;
    @JsonProperty("type")
    private String type;
    @JsonProperty("version")
    private Long version;
    @JsonProperty("url")
    private String url;
    @JsonProperty("public_id")
    private String publicId;
    @JsonProperty("tags")
    private List<Object> tags = null;
    @JsonProperty("original_filename")
    private String originalFilename;
    @JsonProperty("api_key")
    private String apiKey;
    @JsonProperty("bytes")
    private Long bytes;
    @JsonProperty("width")
    private Long width;
    @JsonProperty("etag")
    private String etag;
    @JsonProperty("placeholder")
    private Boolean placeholder;
    @JsonProperty("height")
    private Long height;
}