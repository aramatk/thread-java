package com.threadjava.users.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserUpdateResponse {
    private UserDetailsDto user;
    private String error;

    public UserUpdateResponse(String error) {
        this.error = error;
    }

    public UserUpdateResponse(UserDetailsDto user) {
        this.user = user;
    }
}
