package com.threadjava.users.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class UserResetPasswordDTO {
    private UUID userId;
    private String password;
}
