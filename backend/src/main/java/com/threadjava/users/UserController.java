package com.threadjava.users;

import com.threadjava.image.ImageRepository;
import com.threadjava.image.model.Image;
import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.dto.UserUpdateResponse;
import com.threadjava.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UsersService userDetailsService;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private ImageRepository imageRepository;

    @GetMapping
    public UserDetailsDto getUser() {
        return userDetailsService.getUserById(getUserId());
    }

    @PutMapping
    public UserUpdateResponse updateUser(@RequestParam UUID userId,
                                         @RequestParam String username,
                                         @RequestParam(required = false) UUID imageId,
                                         @RequestParam(required = false) String status) {

        Optional<User> userByUsername = usersRepository.findByUsername(username);
        if (userByUsername.isPresent() && !userByUsername.get().getId().equals(userId)) {
            return new UserUpdateResponse(String.format("User with username %s already exists", username));
        }
        Optional<User> user = usersRepository.findById(userId);
        user.get().setUsername(username);
        user.get().setStatus(status);
        if (imageId != null) {
            Optional<Image> avatar = imageRepository.findById(imageId);
            if (avatar.isPresent()) {
                user.get().setAvatar(avatar.get());
            }
        }
        UserDetailsDto userDetailsDto = userDetailsService.save(user.get());
        return new UserUpdateResponse(userDetailsDto);
    }
}
