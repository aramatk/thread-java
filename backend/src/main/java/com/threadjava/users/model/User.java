package com.threadjava.users.model;

import com.threadjava.db.BaseEntity;
import com.threadjava.image.model.Image;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Data
@EqualsAndHashCode(callSuper=true)
@Table(name = "users")
public class User extends BaseEntity {
    @Column(name = "email", unique=true)
    @NotBlank(message = "Email must be populated")
    @Pattern(regexp = "^(.+)@(.+)$", message = "Please provide a valid email address")
    private String email;

    @NotBlank(message = "Username must be populated")
    @Column(name = "username", unique=true)
    private String username;

    @Column(name = "password")
    private String password;

    @Size(max = 100, message =
            "Status must be less than 100 characters")
    @Column(name = "status")
    private String status;

    @ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "avatar_id")
    private Image avatar;
}
