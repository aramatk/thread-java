package com.threadjava.auth.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ValidateResetPasswordTokenResponse {
    private UUID userId;
    private String error;

    public ValidateResetPasswordTokenResponse() {
    }

    public ValidateResetPasswordTokenResponse(UUID userId) {
        this.userId = userId;
    }

    public ValidateResetPasswordTokenResponse(UUID userId, String error) {
        this.userId = userId;
        this.error = error;
    }
}
