package com.threadjava.auth;

import com.threadjava.users.UsersRepository;
import com.threadjava.users.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

import static com.threadjava.config.SecurityConstants.PASSWORD_RESET_EXPIRATION_TIME;

@Service
public class ResetPasswordTokenService {
    private static final Logger log = LoggerFactory.getLogger(ResetPasswordTokenService.class);
    private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;

    @Autowired
    UsersRepository userRepository;

    public boolean isTokenExpired(String token) {
        Optional<User> user = getUser(token);
        if (user.isPresent()) {
            SecretKey secretKey = getSecretKey(user.get().getPassword().getBytes());
            Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
            Date expirationDate = claims.getExpiration();
            return expirationDate.before(new Date());
        }
        return false;
    }

    public String generateResetPasswordToken(User user) {
        SecretKey key = getSecretKey(user.getPassword().getBytes());
        return Jwts.builder()
                .setClaims(new HashMap<>())
                .setSubject(user.getId().toString())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + PASSWORD_RESET_EXPIRATION_TIME))
                .signWith(key, SIGNATURE_ALGORITHM)
                .compact();
    }

//    public boolean validateSignature(String token) {
//        try {
//            User user = getUser(token);
//            SecretKey secretKey = getSecretKey(user.getPassword().getBytes());
//            DefaultJwtSignatureValidator validator =
//                    new DefaultJwtSignatureValidator(SIGNATURE_ALGORITHM, secretKey, Decoders.BASE64URL);
//            String[] chunks = token.split("\\.");
//            String tokenWithoutSignature = chunks[0] + "." + chunks[1];
//            String signature = chunks[2];
//            return validator.isValid(tokenWithoutSignature, signature);
//        } catch (Exception e) {
//            log.info("Exception occurred during validation token {} {}", token, e.getMessage());
//            return false;
//        }
//    }

    private SecretKey getSecretKey(byte[] secretKey) {
        return Keys.hmacShaKeyFor(secretKey);
    }

    public Optional<User> getUser(String token) {
        try {
            String[] chunks = token.split("\\.");
            String payload = new String(Decoders.BASE64URL.decode(chunks[1]));
            Object obj = new JSONParser().parse(payload);
            JSONObject jo = (JSONObject) obj;
            String userId = (String) jo.get(Claims.SUBJECT);
            Optional<User> user = userRepository.findById(UUID.fromString(userId));
            return user;
        } catch (Exception e) {
            log.info("Exception occurred during extracting user from token {} {}", token, e.getMessage());
            return Optional.empty();
        }
    }
}