package com.threadjava.auth;

import com.threadjava.auth.dto.ValidateResetPasswordTokenResponse;
import com.threadjava.common.dto.GenericResponse;
import com.threadjava.email.IMailService;
import com.threadjava.email.dto.SendMailRequest;
import com.threadjava.users.UsersRepository;
import com.threadjava.users.UsersService;
import com.threadjava.users.dto.UserResetPasswordDTO;
import com.threadjava.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Optional;

@RestController
@RequestMapping("/api/reset/password")
public class ResetPasswordController {
    @Autowired
    private UsersService userDetailsService;
    @Autowired
    private UsersRepository userRepository;
    @Autowired
    private ResetPasswordTokenService resetPasswordTokenService;
    @Autowired
    private IMailService mailService;
    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;
    @Value(value = "${window.location.origin}")
    private String WINDOW_LOCATION_ORIGIN;

    @PostMapping
    public GenericResponse resetPassword(@RequestParam("email") String userEmail) {
        Optional<User> user = userRepository.findByEmail(userEmail);
        if (!user.isPresent()) {
            return new GenericResponse("Error occurred", "The user with given email is not found");
        }

        String token = resetPasswordTokenService.generateResetPasswordToken(user.get());
        mailService.sendEmail(constructResetTokenEmailRequest(token, user.get()));
        return new GenericResponse("Reset password mail was sent!");
    }

    private SendMailRequest constructResetTokenEmailRequest(String token, User user) {
        String url = WINDOW_LOCATION_ORIGIN + "/reset/password/" + token;
        String message = "Hello!<br>To reset password please follow the link " + url;
        return SendMailRequest.builder()
                .subject("Reset password")
                .body(message)
                .recipients(Arrays.asList(user.getEmail()))
                .build();
    }

    @PutMapping
    public GenericResponse changePassword(@RequestBody UserResetPasswordDTO userResetPasswordDTO) {
        Optional<User> user = userRepository.findById(userResetPasswordDTO.getUserId());
        if (!user.isPresent()) {
            return new GenericResponse("Error occurred", "The user with given id is not found");
        }
        user.get().setPassword(bCryptPasswordEncoder.encode(userResetPasswordDTO.getPassword()));
        userDetailsService.save(user.get());
        return new GenericResponse("Password was reset");
    }

    @GetMapping("/{token}")
    public ValidateResetPasswordTokenResponse validateToken(@PathVariable String token) {
        ValidateResetPasswordTokenResponse invalidResponse =
                new ValidateResetPasswordTokenResponse(null, "Password reset link is invalid!");
        try {
            if (!resetPasswordTokenService.isTokenExpired(token)) {
                Optional<User> user = resetPasswordTokenService.getUser(token);
                return user.isPresent() ? new ValidateResetPasswordTokenResponse(user.get().getId()) : invalidResponse;
            }
            return invalidResponse;
        } catch (Exception e) {
            return invalidResponse;
        }
    }
}
