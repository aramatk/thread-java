package com.threadjava.post.dto;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class PostCommentDto {
    private UUID id;
    private UUID postId;
    private UUID userId;
    private String body;
    private PostUserDto user;
    private Date createdAt;
    private Date updatedAt;
    public long likeCount;
    public long dislikeCount;
}
