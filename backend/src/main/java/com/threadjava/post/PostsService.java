package com.threadjava.post;

import com.threadjava.comment.CommentRepository;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostsService {
    @Autowired
    private PostsRepository postsCrudRepository;
    @Autowired
    private CommentRepository commentRepository;

    public List<PostListDto> getAllPosts(Integer from, Integer count, UUID userId, UUID excludeUserId, UUID showLikedByUserId) {
        var pageable = PageRequest.of(from / count, count);
        return postsCrudRepository
                .findAllPosts(userId, excludeUserId, showLikedByUserId, pageable)
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .collect(Collectors.toList());
    }

    public PostDetailsDto getPostById(UUID id) {
        var post = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();

        var comments = commentRepository.findAllByPostId(id)
                .stream()
                .map(PostMapper.MAPPER::commentToCommentDto)
                .collect(Collectors.toList());
        post.setComments(comments);

        return post;
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        Post post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
        Post postCreated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
    }

    public UUID delete(UUID id) {
        Validate.notNull(id, "Provided post id is NULL!");
        var post = postsCrudRepository.findById(id);
        if (post.isPresent() && post.get().getDeletedOn() == null) {
            post.get().setDeletedOn(LocalDateTime.now());
            Post savedPost = postsCrudRepository.save(post.get());
            return savedPost.getId();
        }
        return null;
    }

    public UUID update(PostUpdateDto postUpdateDto) {
        Validate.notNull(postUpdateDto.getId(), "Provided post id is NULL!");
        var post = postsCrudRepository.findById(postUpdateDto.getId());
        if (post.isPresent()) {
            post.get().setBody(postUpdateDto.getBody());
            Post savedPost = postsCrudRepository.save(post.get());
            return savedPost.getId();
        }
        return null;
    }
}
