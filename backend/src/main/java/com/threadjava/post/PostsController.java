package com.threadjava.post;


import com.threadjava.post.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue = "0") Integer from,
                                 @RequestParam(defaultValue = "10") Integer count,
                                 @RequestParam(required = false) UUID excludeUserId,
                                 @RequestParam(required = false) UUID showLikedByUserId,
                                 @RequestParam(required = false) UUID userId) {
        return postsService.getAllPosts(from, count, userId, excludeUserId, showLikedByUserId);
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/post/new", item);
        return item;
    }

    @DeleteMapping("/{id}")
    public UUID delete(@PathVariable UUID id) {
        UUID postId = postsService.delete(id);
        template.convertAndSend("/topic/post/remove", postsService.getPostById(postId));
        return postId;
    }

    @PutMapping
    public UUID update(@RequestBody PostUpdateDto postUpdateDto) {
        UUID postId = postsService.update(postUpdateDto);
        template.convertAndSend("/topic/post/update", postsService.getPostById(postId));
        return postId;
    }
}
