package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.dto.CommentUpdateDto;
import com.threadjava.post.PostsService;
import com.threadjava.post.dto.PostDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping("/{id}")
    public CommentDetailsDto get(@PathVariable UUID id) {
        return commentService.getCommentById(id);
    }

    @PostMapping
    public CommentDetailsDto save(@RequestBody CommentSaveDto commentDto) {
        commentDto.setUserId(getUserId());
        CommentDetailsDto commentDetailsDto = commentService.create(commentDto);

        PostDetailsDto postDetailsDto = postsService.getPostById(commentDto.getPostId());
        UUID postUserId = postDetailsDto.getUser().getId();
        if (!getUserId().equals(postUserId)) {
            // notify a user if someone (not himself) liked his comment
            template.convertAndSendToUser(
                    postUserId.toString(),
                    "/post/comment",
                    "Your post was commented!"
            );
        }
        template.convertAndSend("/topic/comment/new", commentDetailsDto);
        return commentDetailsDto;
    }

    @DeleteMapping("/{id}")
    public UUID delete(@PathVariable UUID id) {
        UUID commentId = commentService.delete(id);
        template.convertAndSend("/topic/comment/remove", commentService.getCommentById(commentId));
        return commentId;
    }

    @PutMapping
    public UUID update(@RequestBody CommentUpdateDto commentUpdateDto) {
        UUID commentId = commentService.update(commentUpdateDto);
        template.convertAndSend("/topic/comment/update", commentService.getCommentById(commentId));
        return commentId;
    }
}
