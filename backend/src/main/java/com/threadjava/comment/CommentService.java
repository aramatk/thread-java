package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.dto.CommentUpdateDto;
import com.threadjava.comment.model.Comment;
import com.threadjava.post.PostsRepository;
import com.threadjava.users.UsersRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;

    public CommentDetailsDto getCommentById(UUID id) {
        return commentRepository.findCommentById(id)
                .map(CommentMapper.MAPPER::commentToCommentDetailsDto)
                .orElseThrow();
    }

    public CommentDetailsDto create(CommentSaveDto commentDto) {
        var comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        var commentCreated = commentRepository.save(comment);
        return getCommentById(commentCreated.getId());
    }

    public UUID delete(UUID id) {
        Validate.notNull(id, "Provided comment id is NULL!");
        var comment = commentRepository.findById(id);
        if (comment.isPresent() && comment.get().getDeletedOn() == null) {
            comment.get().setDeletedOn(LocalDateTime.now());
            Comment savedComment = commentRepository.save(comment.get());
            return savedComment.getId();
        }
        return null;
    }

    public UUID update(CommentUpdateDto commentUpdateDto) {
        Validate.notNull(commentUpdateDto.getId(), "Provided comment id is NULL!");
        var comment = commentRepository.findById(commentUpdateDto.getId());
        if (comment.isPresent()) {
            comment.get().setBody(commentUpdateDto.getBody());
            Comment savedComment = commentRepository.save(comment.get());
            return savedComment.getId();
        }
        return null;
    }
}
