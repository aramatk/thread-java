package com.threadjava.postReactions;

import com.threadjava.postReactions.model.PostReaction;
import com.threadjava.users.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PostReactionsRepository extends CrudRepository<PostReaction, UUID> {
    @Query("SELECT r " +
            "FROM PostReaction r " +
            "WHERE r.user.id = :userId AND r.post.id = :postId ")
    Optional<PostReaction> getPostReaction(@Param("userId") UUID userId, @Param("postId") UUID postId);

    @Query("SELECT distinct r.user " +
            "FROM PostReaction r " +
            "WHERE r.isLike = :isLiked AND r.post.id = :postId ")
    List<User> getUsersWithReactions(@Param("postId") UUID postId, @Param("isLiked") Boolean isLiked);
}