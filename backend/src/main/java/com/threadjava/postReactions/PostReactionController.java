package com.threadjava.postReactions;

import com.threadjava.email.IMailService;
import com.threadjava.email.dto.SendMailRequest;
import com.threadjava.postReactions.dto.NotificationPostReactionDto;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import com.threadjava.users.UserMapper;
import com.threadjava.users.UsersRepository;
import com.threadjava.users.dto.UserDetailsDto;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/postreaction")
public class PostReactionController {
    @Autowired
    private PostReactionService postsService;
    @Autowired
    private PostReactionsRepository postReactionsRepository;
    @Autowired
    private SimpMessagingTemplate template;
    @Autowired
    private IMailService mailService;
    @Autowired
    private UsersRepository usersRepository;
    @Value(value = "${window.location.origin}")
    private String WINDOW_LOCATION_ORIGIN;

    @PutMapping
    public Optional<ResponsePostReactionDto> setReaction(@RequestBody ReceivedPostReactionDto postReaction) {
        postReaction.setUserId(getUserId());
        var reaction = postsService.setReaction(postReaction);

        if (reaction.isPresent() && !getUserId().equals(reaction.get().getAuthorId())
                && Boolean.TRUE.equals(reaction.get().getIsLike())) {
            // notify a user if someone (not himself) liked his post
            template.convertAndSendToUser(
                    reaction.get().getAuthorId().toString(),
                    "/like",
                    "Your post was liked!"
            );

            sendNotificationEmail(reaction.get());
        }
        template.convertAndSend("/topic/post/react", NotificationPostReactionDto.builder()
                .userId(getUserId())
                .postId(postReaction.getPostId())
                .isLike(postReaction.getIsLike())
                .reaction(reaction)
                .build());
        return reaction;
    }

    private void sendNotificationEmail(ResponsePostReactionDto reaction) {
        var likedUser = usersRepository.findById(getUserId()).get();
        var author = usersRepository.findById(reaction.getAuthorId()).get();
        SendMailRequest sendMailRequest = SendMailRequest.builder()
                .subject("Your post was liked!")
                .body(String.format("Hello!<br>Your post was liked by %s.<br>Post: %s", likedUser.getUsername(),
                        String.format("%s/share/%s}", WINDOW_LOCATION_ORIGIN, reaction.getPostId())))
                .recipients(Arrays.asList(author.getEmail())).build();
        mailService.sendEmail(sendMailRequest);
    }

    @GetMapping("/{postId}/{isLike}")
    public List<UserDetailsDto> getUsersWithReaction(@PathVariable UUID postId, @PathVariable Boolean isLike) {
        Validate.notNull(postId, "Post id must not be null!");
        Validate.notNull(isLike, "isLike must not be null!");

        return postReactionsRepository.getUsersWithReactions(postId, isLike).stream()
                .map(u -> UserMapper.MAPPER.userToUserDetailsDto(u))
                .collect(Collectors.toList());
    }
}
