package com.threadjava.postReactions;

import com.threadjava.post.PostsService;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import com.threadjava.postReactions.model.PostReaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PostReactionService {
    @Autowired
    private PostReactionsRepository postReactionsRepository;

    @Autowired
    private PostsService postsService;

    public Optional<ResponsePostReactionDto> setReaction(ReceivedPostReactionDto postReactionDto) {

        var reaction = postReactionsRepository.getPostReaction(postReactionDto.getUserId(), postReactionDto.getPostId());

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike() == postReactionDto.getIsLike()) {
                postReactionsRepository.deleteById(react.getId());
                return Optional.empty();
            } else {
                react.setIsLike(postReactionDto.getIsLike());
                Optional<ResponsePostReactionDto> responsePostReactionDto = save(react);
                responsePostReactionDto.get().setReactionChanged(true);
                return responsePostReactionDto;
            }
        } else {
            var postReaction = PostReactionMapper.MAPPER.dtoToPostReaction(postReactionDto);
            return save(postReaction);
        }
    }

    private Optional<ResponsePostReactionDto> save(PostReaction newReaction) {
        var reaction = postReactionsRepository.save(newReaction);
        var post = postsService.getPostById(reaction.getPost().getId());
        return Optional.of(
                ResponsePostReactionDto
                        .builder()
                        .id(reaction.getId())
                        .isLike(reaction.getIsLike())
                        .userId(reaction.getUser().getId())
                        .postId(post.getId())
                        .authorId(post.getUser().getId())
                        .reactionChanged(false)
                        .build()
        );
    }
}
