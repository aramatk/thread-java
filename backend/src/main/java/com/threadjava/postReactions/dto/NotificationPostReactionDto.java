package com.threadjava.postReactions.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;
import java.util.UUID;

@Data
@Builder
public class NotificationPostReactionDto {
    private UUID userId;
    private UUID postId;
    private Boolean isLike;
    private Optional<ResponsePostReactionDto> reaction;
}
